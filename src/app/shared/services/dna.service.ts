import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Dna } from '../models/dna';

//Servicio para el dna
@Injectable()
export class DNAService {
  httpOptions = {
    headers: new HttpHeaders({

    }),
  };
  constructor(protected http: HttpClient) {}

  //Obtener consulta de ADN
  queryMutation(dna):Observable<any>{
      return this.http.post<any>(`${environment.serverApiUrl}/mutation`, dna)
  }

  //Obtener lista de los 10 ultimos ADN consultados
  getList(): Observable<Dna[]> {
    return this.http.get<Dna[]>(`${environment.serverApiUrl}/list`);
  }

  //Obtener Status
  getStats(): Observable<any> {
    return this.http.get<any>(`${environment.serverApiUrl}/stats`);
  }
}
