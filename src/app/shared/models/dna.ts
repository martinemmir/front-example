//Modelo
export class Dna{
    id: number;
    dna: string;
    isMutation:Boolean;
    saveDate: Date;
}