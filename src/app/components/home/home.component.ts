import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { DNAService } from 'src/app/shared/services/dna.service';
import { HeaderService } from 'src/app/shared/services/header.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  dnas;
  inputDna;
  showListSpinner = true;
  
  constructor(
    private dnaService: DNAService,
    private headerService: HeaderService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.getList();
  }

  //Obtenemos la lista de los 10 ultimos ADN
  getList() {

    //Esto es para mostrar el spinner la primera vez que entras a la pagina
    if (this.showListSpinner == true) {
      this.spinner.show();
    }
    this.dnaService.getList().subscribe(
      (x) => {
        this.dnas = x;
        this.spinner.hide();
        this.showListSpinner = false;
      },
      (error) => {
        console.log(error);
        this.spinner.hide();
        this.showListSpinner = false;
      }
    );
  }

  //Evento que se desata al dar click en el boton consultar
  onClick() {
    if (this.inputDna != undefined && this.inputDna != '') {
      //Se muestra el spinner
      this.spinner.show();

      //Se transforma el ADN en mayusculas
      this.inputDna = this.inputDna.toUpperCase();
      
      //Se transforma el ADN en una matriz
      var matriz = this.inputDna.split(',');
      var adn = { dna: matriz };

      this.dnaService.queryMutation(adn).subscribe(
        (x) => {

          //Si la consulta regresa un status 200 es que es una mutación
          this.headerService.setTitle();
          this.getList();
          Swal.fire({
            icon: 'success',
            title: 'Es una Mutación',
          });
        },
        (error) => {
          //Si la consulta regresa un status 403 es que no es una mutación
          if (error.status == 403) {
            this.headerService.setTitle();
            this.getList();
            Swal.fire({
              icon: 'error',
              title: 'No es una Mutación',
            });
          }

          if (error.status == 500) {
            this.spinner.hide();
            console.log(error);
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Ocurrio un error interno'
            })
          }
        }
      );
    }
  }

  //Validamos que solo se puedan introducir las letras A T C G
  public inputValidator(event: any) {
    const pattern = /^[A|T|C|G|a|t|c|g|,|]*$/;
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(
        /[^A|T|C|G|a|t|c|g|,|]/g,
        ''
      );
    }
    this.inputDna = event.target.value;
  }
}
