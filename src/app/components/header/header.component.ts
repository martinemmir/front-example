import { Component, OnInit } from '@angular/core';
import { HeaderService } from 'src/app/shared/services/header.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  stats;
  constructor(private headerService: HeaderService) { 
    this.getStats();
  }

  ngOnInit(): void {
  }

  //Sirve para actualizar los stats del navbar
  getStats(){
    this.headerService.setTitle();
    this.headerService.title.subscribe((title) => {
      this.stats = title;
    });
  }
}
